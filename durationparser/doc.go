/*
Package durationparser contains a duration parser created to have a wider range of units than the stdlib time.ParseDuration and to attempt to take into account the variability in calender units such as months or years

To do this it uses a time.Time reference and custom transform functions for certain units instead of a duration sum with fixed units,
as such all durations are parsed relative to a point in time, using `ParseAt` you can specify what time to use, or you can use `Parse` to parse relative to time.Now or `ParseStatic` to parse relative to a blank time.Time{}

It also includes some extra customisability like the ability to create custom units or set a max or minimum duration
*/
package durationparser
