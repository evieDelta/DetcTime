package durationparser

import "time"

// Some default English terms for time
var (
	EnglishIdentifiersMiliseconds = []string{"milliseconds", "millisecond", "miliseconds", "milisecond", "ms"}
	EnglishIdentifiersSeconds     = []string{"seconds", "second", "secs", "sec", "s"}
	EnglishIdentifiersMinutes     = []string{"minutes", "minute", "mins", "min", "m"}
	EnglishIdentifiersHours       = []string{"hours", "hour", "h"}
	EnglishIdentifiersDays        = []string{"days", "day", "d"}
	EnglishIdentifiersWeeks       = []string{"weeks", "week", "wks", "wk", "w"}
	EnglishIdentifiersMonths      = []string{"months", "month", "mo"}
	EnglishIdentifiersQuarters    = []string{"quarters", "quarter", "q"}
	EnglishIdentifiersYears       = []string{"years", "year", "y"}
	EnglishIdentifiersDecades     = []string{"decades", "decade", "dec"}
)

// Preset english units
var (
	Millisecond = Unit{
		Unit:        StaticAdder(time.Millisecond),
		Identifiers: EnglishIdentifiersMiliseconds,
	}
	Second = Unit{
		Unit:        StaticAdder(time.Second),
		Identifiers: EnglishIdentifiersSeconds,
	}
	Minute = Unit{
		Unit:        StaticAdder(time.Minute),
		Identifiers: EnglishIdentifiersMinutes,
	}
	Hour = Unit{
		Unit:        StaticAdder(time.Hour),
		Identifiers: EnglishIdentifiersHours,
	}
	Day = Unit{
		Unit:        DayAdder(1),
		Identifiers: EnglishIdentifiersDays,
	}
	Week = Unit{
		Unit:        DayAdder(7),
		Identifiers: EnglishIdentifiersWeeks,
	}
	Month = Unit{
		Unit:        MonthAdder(1),
		Identifiers: EnglishIdentifiersMonths,
	}
	Year = Unit{
		Unit:        YearAdder(1),
		Identifiers: EnglishIdentifiersYears,
	}
	Decade = Unit{
		Unit:        YearAdder(10),
		Identifiers: EnglishIdentifiersDecades,
	}
)
