package durationparser

import (
	"math"
	"strconv"
	"strings"
	"time"
	"unicode"
)

// Parser is a duration parsing tool that uses the calander via time.Time as a reference point instead of a static length sum.
// the goal being that by using time.Time as a base instead of a duration it should be able to properly take into account the variable length of calender units,
// so that if its may 4th a duration string such as `2 months` returns the duration of time between may 4th and july 4th instead of just assuming all months are the same and returning 60 days.
// Parser is safe for concurrent use as long as the units or settings are not modified while in use and no custom unit methods are created with concurrency unsafe code.
type Parser struct {
	// The units names and their assosiated functions to use in parsing a duration
	Units []Unit

	// The Maximum duration, if Max != 0 it will check if the parsed duration exceeds max and will return the value of max and ErrorTooHigh
	// ErrorTooHigh can be safely ignored as the returned duration will be the value of max (check for ErrorTooHigh using errors.Unwrap)
	Maximum time.Duration

	// The minimum duration, functions the same as maximum except flipped to check if the duration is less than minimum and returns ErrorTooLow instead
	Minimum time.Duration

	// the unit to use if no unit is specified
	DefaultUnit string

	// This sets whether or not spaces should be completely ignored (false) or whether they should be kept in unit identifiers
	// when disabled (default) `1 0 h o u r s` gets parsed the same as 10 `hours`
	// when enabled it gets parsed as 10 `h o u r s`
	KeepSpacesInIdentifier bool
}

// Parse is equivalent to calling p.ParseAt with time.Now() as t
func (p Parser) Parse(s string) (time.Duration, error) {
	return p.ParseAt(s, time.Now())
}

// ParseStatic is equivalent to calling p.ParseAt with a blank time.Time{}
func (p Parser) ParseStatic(s string) (time.Duration, error) {
	return p.ParseAt(s, time.Time{})
}

// ParseAt parses a duration string relative to the time given in t.
func (p Parser) ParseAt(s string, t time.Time) (time.Duration, error) {
	// Parse syntax
	blks := p.divider(s)
	timesum := t

	// Range through the syntax blocks
	for _, x := range blks {
		// Find the unit, or return the unit not found error if there is none
		unit, err := p.findUnit(x.identifier)
		if err != nil {
			return 0, err
		}
		// Parse the number
		ni, nf, err := p.parseNumber(x.number)
		if err != nil {
			return 0, err
		}
		// Pass it to the units addition function
		timesum = unit.Add(timesum, ni, nf)
	}

	// Check the maximum and minimum
	d := timesum.Sub(t)
	if p.Maximum != 0 && d > p.Maximum {
		return p.Maximum, newErrorf(ErrorTooHigh, "Got: %v, Maximum: %v", d, p.Maximum)
	}
	if p.Minimum != 0 && d < p.Minimum {
		return p.Minimum, newErrorf(ErrorTooLow, "Got: %v, Minimum: %v", d, p.Minimum)
	}

	return d, nil
}

// findUnit searches the units within the host parser for a unit identifier matching id
func (p *Parser) findUnit(id string) (UnitAdder, error) {
	// if there is no set unit set it to whatever is in p.DefaultUnit (which could also be blank)
	// one could also set a default by just defining a unit with "" as an identifier, but the struct field is a simpler api
	if id == "" {
		id = p.DefaultUnit
	}

	// probably a faster way to do this but this'll work for now
	for _, x := range p.Units {
		for _, y := range x.Identifiers {
			if id == y {
				return x.Unit, nil
			}
		}
	}
	return nil, &dpError{
		SubError: ErrorUndefinedUnit,
		Message:  id,
	}
}

// determine if a number has a decimal point and parse it as either a float or an int depending on such
func (p *Parser) parseNumber(s string) (i int, f float64, err error) {
	// so i'm not explicitly checking for an error here because i don't think the conversion from float to int can cause any issues even if its NaN or inf, and there is nothing else in the function after it so its gonna be returned right after if there was one
	if strings.ContainsRune(s, '.') {
		f, err = strconv.ParseFloat(s, 64)
		i = int(math.Round(f))
	} else {
		i, err = strconv.Atoi(s)
		f = float64(i)
	}
	return
}

// thingy for keeping info on a piece of unit
type numidgroup struct {
	number     string
	identifier string
}

func (p Parser) divider(s string) []numidgroup {
	// State variables
	var blocks = make([]numidgroup, 0, 10)
	var nbuffer, ubuffer string
	var state bool

	// this is used like 2 times so instead of repeating code just make it an anonymous function
	finishblock := func() {
		if nbuffer == "" && ubuffer == "" {
			return
		}

		// If its set to allow spaces in the identifier trim all trailing and leading spaces
		if p.KeepSpacesInIdentifier {
			ubuffer = strings.TrimSpace(ubuffer)
		}

		blocks = append(blocks, numidgroup{
			number:     nbuffer,
			identifier: ubuffer,
		})

		state = false
		nbuffer, ubuffer = "", ""
	}

	// Loop through the string
	for _, x := range s {
		//fmt.Println(state, string(x), "|", nbuffer, "|", ubuffer)

		// If the char is a space continue (spaces are not really needed so we can ignore them
		// yeah it means `1 0 h o u r s` gets parsed into 10 hours (unless p.KeepSpacesInIdentifier is enabled which in that case it becomes 10 h o u r s) but whatever)
		if unicode.IsSpace(x) && !(p.KeepSpacesInIdentifier && state) {
			continue
		}

		if !state {
			// If state is false it is in the initial number collection phase

			if unicode.IsNumber(x) || x == '.' || x == '-' {
				// Add char to num buffer if its a number
				nbuffer += string(x)
			} else {
				// Otherwise flip the state to the unit collection phase and add it to the unit buffer
				state = true
				ubuffer += string(x)
			}
		} else {
			// If state is true then it is in the unit collection phase

			if unicode.IsNumber(x) || x == '.' || x == '-' {
				// If its a number then finalise the current item and reset the buffers
				finishblock()

				// and add the stray character to the number buffer
				nbuffer += string(x)
			} else {
				// If it is not a number add it to the unit buffer
				ubuffer += string(x)
			}
		}
	}

	// Finalise the last item if there is anything remaining
	if nbuffer != "" || ubuffer != "" {
		finishblock()
	}

	return blocks
}
