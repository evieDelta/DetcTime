package durationparser_test

import (
	"errors"
	"fmt"
	"testing"
	"time"

	"codeberg.org/eviedelta/detctime/durationparser"
)

// Some unit tests for the duration parser
// Note that these tests are a little messy and unlike the lib itself are not commented

var units = []durationparser.Unit{
	durationparser.Millisecond,
	durationparser.Second,
	durationparser.Minute,
	durationparser.Hour,
	durationparser.Day,
	durationparser.Week,
	durationparser.Month,
	durationparser.Year,
	durationparser.Decade,
}

type testDuration struct {
	String   string
	Expected time.Duration
}

func boolToOk(b bool) string {
	if b {
		return "okay"
	}
	return "failed"
}

func TestParser(t *testing.T) {
	ok := t.Run("Static Simple", testParserStaticSimple)
	fmt.Println("Static Simple:", boolToOk(ok))

	ok = t.Run("Static FP and Negatives", testParserStaticFP)
	fmt.Println("Static FP and Negatives:", boolToOk(ok))

	ok = t.Run("Calander Durations", testParserCalender)
	fmt.Println("Calander Durations:", boolToOk(ok))

	ok = t.Run("Default Units:", testParserDefaultUnits)
	fmt.Println("Default Units:", boolToOk(ok))

	ok = t.Run("Max Checker:", testParserMaxDuration)
	fmt.Println("Max Checker:", boolToOk(ok))

	ok = t.Run("Min Checker:", testParserMinDuration)
	fmt.Println("Min Checker:", boolToOk(ok))
}

func testParserStaticSimple(t *testing.T) {
	p := &durationparser.Parser{
		Units: units,
	}

	// Basic tests of basic static duration
	tests := []testDuration{
		{
			String:   "100ms",
			Expected: time.Millisecond * 100,
		},
		{
			String:   "30s",
			Expected: time.Second * 30,
		},
		{
			String:   "5m",
			Expected: time.Minute * 5,
		},
		{
			String:   "5 minutes",
			Expected: time.Minute * 5,
		},
		{
			String:   "1 hour",
			Expected: time.Hour,
		},
		{
			String:   "2h30m",
			Expected: (time.Hour * 2) + (time.Minute * 30),
		},
		{
			String:   "2 hours 30 minutes",
			Expected: (time.Hour * 2) + (time.Minute * 30),
		},
		{
			String:   "2hours 5 m",
			Expected: (time.Hour * 2) + (time.Minute * 5),
		},
		{
			String:   "1 hour 3m5s 140ms",
			Expected: (time.Hour) + (time.Minute * 3) + (time.Second * 5) + (time.Millisecond * 140),
		},
		{
			String:   "5s 5s",
			Expected: (time.Second * 10),
		},
	}

	for _, x := range tests {
		d, err := p.Parse(x.String)
		if err != nil {
			t.Errorf("Test `%v` failed with error: %v", x.String, err)
			continue
		}
		if d != x.Expected {
			t.Errorf("Test `%v` failed: got: %v expected: %v", x.String, d, x.Expected)
		}
	}
}

func testParserStaticFP(t *testing.T) {
	p := &durationparser.Parser{
		Units: units,
	}

	// Tests of static duration units but with fp and negatives
	tests := []testDuration{
		{
			String:   "30.5s",
			Expected: time.Second*30 + (time.Second / 2),
		},
		{
			String:   "1.5m",
			Expected: time.Minute*1 + (time.Second * 30),
		},
		{
			String:   "-1.5 hour",
			Expected: -time.Hour + (-time.Hour / 2),
		},
		{
			String:   "2h30.5m",
			Expected: (time.Hour * 2) + (time.Minute*30 + (time.Second * 30)),
		},
		{
			String:   "0.5s 0.5s",
			Expected: (time.Second),
		},
	}

	for _, x := range tests {
		d, err := p.Parse(x.String)
		if err != nil {
			t.Errorf("Test `%v` failed with error: %v", x.String, err)
			continue
		}
		if d != x.Expected {
			t.Errorf("Test `%v` failed: got: %v expected: %v", x.String, d, x.Expected)
		}
	}
}

func testParserCalender(t *testing.T) {
	p := &durationparser.Parser{
		Units: units,
	}
	tm, err := time.ParseInLocation(time.RFC3339, "2020-05-04T00:04:35+00:00", time.UTC)
	if err != nil {
		t.Fatal(err)
	}

	// Tests for calander duration units
	tests := []struct {
		String   string
		Expected func(time.Duration) (string, bool)
	}{
		{
			String: "1d",
			Expected: func(t time.Duration) (string, bool) {
				nt := tm.AddDate(0, 0, 1)
				return fmt.Sprintf("%v / %v",
						nt.Sub(tm).String(),
						nt.Format("2006-01-02 00:04:35"),
					),
					tm.Add(t).Equal(nt)
			},
		},
		{
			String: "1mo",
			Expected: func(t time.Duration) (string, bool) {
				nt := tm.AddDate(0, 1, 0)
				return fmt.Sprintf("%v / %v",
						nt.Sub(tm).String(),
						nt.Format("2006-01-02 00:04:35"),
					),
					tm.Add(t).Equal(nt)
			},
		},
		{
			String: "1 week",
			Expected: func(t time.Duration) (string, bool) {
				nt := tm.AddDate(0, 0, 7)
				return fmt.Sprintf("%v / %v",
						nt.Sub(tm).String(),
						nt.Format("2006-01-02 00:04:35"),
					),
					tm.Add(t).Equal(nt)
			},
		},
		{
			String: "1y",
			Expected: func(t time.Duration) (string, bool) {
				nt := tm.AddDate(1, 0, 0)
				return fmt.Sprintf("%v / %v",
						nt.Sub(tm).String(),
						nt.Format("2006-01-02 00:04:35"),
					),
					tm.Add(t).Equal(nt)
			},
		},
		{
			String: "5y 3mo 2 days",
			Expected: func(t time.Duration) (string, bool) {
				nt := tm.AddDate(5, 3, 2)
				return fmt.Sprintf("%v / %v",
						nt.Sub(tm).String(),
						nt.Format("2006-01-02 00:04:35"),
					),
					tm.Add(t).Equal(nt)
			},
		},
		{
			String: "12y 4 months 5 hours 33m45s",
			Expected: func(t time.Duration) (string, bool) {
				nt := tm.
					AddDate(12, 4, 0).
					Add(time.Hour * 5).
					Add(time.Minute * 33).
					Add(time.Second * 45)
				return fmt.Sprintf("%v / %v",
						nt.Sub(tm).String(),
						nt.Format("2006-01-02 00:04:35"),
					),
					tm.Add(t).Equal(nt)
			},
		},
		{
			String: "12mo",
			Expected: func(t time.Duration) (string, bool) {
				nt := tm.AddDate(1, 0, 0)
				return fmt.Sprintf("%v / %v",
						nt.Sub(tm).String(),
						nt.Format("2006-01-02 00:04:35"),
					),
					tm.Add(t).Equal(nt)
			},
		},
		{
			String: "31d",
			Expected: func(t time.Duration) (string, bool) {
				nt := tm.AddDate(0, 1, 0)
				return fmt.Sprintf("%v / %v",
						nt.Sub(tm).String(),
						nt.Format("2006-01-02 00:04:35"),
					),
					tm.Add(t).Equal(nt)
			},
		},
		{
			String: "1 year 1 week",
			Expected: func(t time.Duration) (string, bool) {
				nt := tm.AddDate(1, 0, 7)
				return fmt.Sprintf("%v / %v",
						nt.Sub(tm).String(),
						nt.Format("2006-01-02 00:04:35"),
					),
					tm.Add(t).Equal(nt)
			},
		},
		{
			String: "1 decade",
			Expected: func(t time.Duration) (string, bool) {
				nt := tm.AddDate(10, 0, 0)
				return fmt.Sprintf("%v / %v",
						nt.Sub(tm).String(),
						nt.Format("2006-01-02 00:04:35"),
					),
					tm.Add(t).Equal(nt)
			},
		},
		{
			String: "1 decade 3 years",
			Expected: func(t time.Duration) (string, bool) {
				nt := tm.AddDate(13, 0, 0)
				return fmt.Sprintf("%v / %v",
						nt.Sub(tm).String(),
						nt.Format("2006-01-02 00:04:35"),
					),
					tm.Add(t).Equal(nt)
			},
		},
	}

	for _, x := range tests {
		d, err := p.ParseAt(x.String, tm)
		if err != nil {
			t.Errorf("Test `%v`@%v failed with error: %v", x.String, tm.Format("2006-01-02 00:04:35"), err)
			continue
		}
		if ex, ok := x.Expected(d); !ok {
			t.Errorf("Test `%v` failed: got: %v @ %v expected: %v", x.String, d, tm.Add(d).Format("2006-01-02 00:04:35"), ex)
		}
	}
}

func testParserDefaultUnits(t *testing.T) {
	p := &durationparser.Parser{
		Units:       units,
		DefaultUnit: "hours",
	}

	d, err := p.ParseStatic("100")
	if err != nil {
		t.Errorf("Failed with error: %v", err)
	}
	if d != time.Hour*100 {
		t.Errorf("Failed: got: %v expected: %v", d, "100h")
	}
}

func testParserMaxDuration(t *testing.T) {
	p := &durationparser.Parser{
		Units:   units,
		Maximum: time.Hour * 24,
	}

	d, err := p.ParseStatic("10h")
	if err != nil {
		t.Errorf("Failed with error: %v", err)
	} else {
		if d != time.Hour*10 {
			t.Errorf("Failed: got: %v expected: %v", d, "10h")
		}
	}

	d, err = p.ParseStatic("2d")
	if err == nil {
		t.Error("Did not return error for duration over maximum, got:", d)
	} else {
		if !errors.Is(err, durationparser.ErrorTooHigh) {
			t.Errorf("Returned error was not ErrorTooHigh, got: %v, duration returned: %v", err, d)
		} else if d != time.Hour*24 {
			t.Errorf("Returned ErrorTooHigh but did not clamp duration, got: %v, expected %v", time.Hour*24, d)
		}
	}

	d, err = p.ParseStatic("24h")
	if err != nil {
		t.Errorf("Failed with error: %v", err)
	} else {
		if d != time.Hour*24 {
			t.Errorf("Failed: got: %v expected: %v", d, "24h")
		}
	}
}

func testParserMinDuration(t *testing.T) {
	p := &durationparser.Parser{
		Units:   units,
		Minimum: time.Hour * 24,
	}

	d, err := p.ParseStatic("30h")
	if err != nil {
		t.Errorf("Failed with error: %v", err)
	} else {
		if d != time.Hour*30 {
			t.Errorf("Failed: got: %v expected: %v", d, "30h")
		}
	}

	d, err = p.ParseStatic("10h")
	if err == nil {
		t.Error("Did not return error for duration under minimum, got:", d)
	} else {
		if !errors.Is(err, durationparser.ErrorTooLow) {
			t.Errorf("Returned error was not ErrorTooLow, got error: %v, duration returned: %v", err, d)
		} else if d != time.Hour*24 {
			t.Errorf("Returned ErrorTooLow but did not set duration to minimum, got: %v, expected %v", time.Hour*24, d)
		}
	}

	d, err = p.ParseStatic("24h")
	if err != nil {
		t.Errorf("Failed with error: %v", err)
	} else {
		if d != time.Hour*24 {
			t.Errorf("Failed: got: %v expected: %v", d, "24h")
		}
	}
}
