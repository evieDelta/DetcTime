package durationparser

import "fmt"

func newError(t error, message ...interface{}) error {
	return &dpError{
		SubError: t,
		Message:  fmt.Sprint(message...),
	}
}

func newErrorf(t error, f string, message ...interface{}) error {
	return &dpError{
		SubError: t,
		Message:  fmt.Sprintf(f, message...),
	}
}

type dpErrorType uint

func (d dpErrorType) Error() string { return errorNameMap[d] }

// Some various error internal error types.
// These can be used with the errors.Is function provided by the errors package to help narrow down the error cause and provide a better formatted error message to a user.
// Note that these only define the errors specific to the parser, errors from other packages (such as a malformed float or int) are not defined or handled by this package, those errors are simply returned as is by the parse function. (see strconv.ErrRange and strconv.ErrSyntax for checking those)
const (
	ErrorUnknownError dpErrorType = iota
	ErrorUndefinedUnit
	ErrorTooHigh
	ErrorTooLow
)

var errorNameMap = map[dpErrorType]string{
	ErrorUnknownError:  "Undefined Error",
	ErrorUndefinedUnit: "Undefined Unit",
	ErrorTooHigh:       "Duration too high",
	ErrorTooLow:        "Duration too low",
}

type dpError struct {
	SubError error
	Message  string
}

func (d *dpError) Error() string {
	return d.SubError.Error() + ": " + d.Message
}

func (d *dpError) Unwrap() error {
	return d.SubError
}
