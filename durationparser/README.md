# DetcTime DurationParser
this is a duration parser created to support a wider range of units and with slightly more flexible syntax than the stdlib time.ParseDuration and to attempt to take into account the variability in calender units such as months or years

To handle calender time this duration parser uses a time.Time reference and custom transform functions for certain units instead of a duration sum with fixed units, as such all durations are parsed relative to a certain point in time

The goal being that by using time.Time as a base instead of a duration it should be able to properly take into account the variable length of calender units (most notably months and years, but also days within the regions that still use daylight savings time) so that if its may 4th a duration string such as `2 months` returns the duration of time between may 4th and july 4th instead of just assuming all months are the same and returning 60 days

using `ParseAt` you can specify what time to use, or you can use `Parse` to parse relative to time.Now or `ParseStatic` to parse relative to a blank time.Time{}

It also includes some extra customisability like the ability to create custom units or set a max or minimum duration

### String formats
it supports typical duration strings like below
* `10d5h`
* `10 days 5 hours`
or any combination of
* `2 days 5h30m`
* `2 weeks 1d 30 hours 5 m`

as long as it is within the pattern of `number` `identifier` `number` `identifier` it'll parse
the only expection being if a default unit is set (or a unit is defined with a blank string as an identifier) it can take a number with no trailing identifier at the end
* `100`
assuming the default is set to minutes that will give 1 hour 40 minutes

spaces are completely ignored while parsing so `1 0 h o u r s` gets parsed as 10 `hours`, unless p.KeepSpacesInIdentifier is enabled which in that case it gets parsed as 10 `h o u r s`

negative numbers `-3 minutes` are supported

decimal points `1.5h` are partly supported,
they are fully supported on units using StaticAdder (all units < day), they will be rounded to the nearest integer number with DayAdder MonthAdder and YearAdder (all units >= day) however

### Examples

Parse a duration relative to the current time
```go
d, err := durationparser.Parse("10 hours 5 minutes")
// 10h5m
```

Parse a duration relative to a blank time.Time{}
```go
d, err := durationparser.ParseStatic("10 hours 5 minutes")
// 10h5m
```

Parse Relative to a custom time
```go
tm, _ := time.Parse(time.RFC3339, "2020-05-04T00:04:35+00:00")

d, err := durationparser.ParseAt("2mo", tm)
// 1464h0m0s, equal to the distance between 2020-05-04 and 2020-7-04
```

Custom parser
```go
par := &durationparser.Parser{
	Units: []durationparser.Unit{
		durationparser.Second,
		durationparser.Minute,
	},
	Maximum: time.Duration*48,
	Minimum: 0,
}
// creates a parser with Minutes and seconds, a minimum disallowing any negative durations, and a max of 2 days
```

Define a custom unit using the StaticAdder (a custom unit can be defined using any type implementing UnitAdder though)
```go
pitime := durationparser.Unit{
	Identifiers: []string{"pi", "pis"},
	Unit:        durationparser.StaticAdder(time.Second * 3141),
}

par := &durationparser.Parser{
	Units: []durationparser.Unit{
		pitime,
	},
}

d, err := par.Parse("2 pis")
// 1h44m42s
```