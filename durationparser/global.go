package durationparser

import "time"

// Default is a presetup default parser
var Default = &Parser{
	Units: []Unit{
		Millisecond,
		Second,
		Minute,
		Hour,
		Day,
		Week,
		Month,
		Year,
		Decade,
	},
}

// Parse is an alias to Parse in the default parser, see the documentation on Parser and its methods for more information
func Parse(s string) (time.Duration, error) {
	return Default.Parse(s)
}

// ParseStatic is an alias to ParseStatic in the default parser, see the documentation on Parser and its methods for more information
func ParseStatic(s string) (time.Duration, error) {
	return Default.ParseStatic(s)
}

// ParseAt is an alias to ParseAt in the default parser, see the documentation on Parser and its methods for more information
func ParseAt(s string, tm time.Time) (time.Duration, error) {
	return Default.ParseAt(s, tm)
}
