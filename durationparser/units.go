package durationparser

import (
	"math"
	"time"
)

// Unit defines a units addition rule (via UnitAdder) and its identifiers
type Unit struct {
	Unit        UnitAdder
	Identifiers []string
}

// UnitAdder is an interface to allow custom means of adding time to things
type UnitAdder interface {
	// t is the time to be added to
	// integer is the number given with the unit (rounded down/tunctuated if any decimal point was given)
	// float is the number given with the unit as a floating point number, including the decimal point if given
	Add(t time.Time, integer int, float float64) time.Time
}

// StaticAdder is a UnitAdder that adds a fixed unit of duration to t
type StaticAdder time.Duration

// Add adds f * fp duration to t and returns the result
func (f StaticAdder) Add(t time.Time, nbr int, fp float64) time.Time {
	return t.Add(time.Duration(
		math.Round(float64(f) * fp),
	))
}

// YearAdder is a UnitAdder that adds a number of years taking account of the calander (via time.Time.AddDate)
type YearAdder int

// Add adds y * nbr years to t
func (y YearAdder) Add(t time.Time, nbr int, fp float64) time.Time {
	return t.AddDate(int(y)*nbr, 0, 0)
}

// MonthAdder is a UnitAdder that adds a number of months taking account of the calander (via time.Time.AddDate)
type MonthAdder int

// Add adds m * nbr months to t
func (m MonthAdder) Add(t time.Time, nbr int, fp float64) time.Time {
	return t.AddDate(0, int(m)*nbr, 0)
}

// DayAdder is a UnitAdder that adds a number of days taking account of the calander (via time.Time.AddDate)
type DayAdder int

// Add adds d * nbr days to t
func (d DayAdder) Add(t time.Time, nbr int, fp float64) time.Time {
	return t.AddDate(0, 0, int(d)*nbr)
}
