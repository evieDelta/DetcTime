detctime is a repository containing some miscellaneous subpackages related to time or duration,
each subpackage is a small mini-library usually designed to do one specific thing.

See the various subpackages/subfolders for the documentation relating to each library